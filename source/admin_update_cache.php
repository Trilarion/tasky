<?php

require_once 'internal/base.php';

session_check();

function compare_tasks($a, $b) {
    global $preferences;
    $a_category = array_search($a['Category'], $preferences['categories'], $strict=TRUE);
    $b_category = array_search($b['Category'], $preferences['categories'], $strict=TRUE);    
    if ($a_category !== $b_category) {
        // first we compare Category
        return $a_category < $b_category ? -1 : 1;
    } else {
        // if category is the same we compare Id
        return $a['Id'] < $b['Id'] ? -1 : 1;
    }
}

function add_category_numbers(&$tasks) {
    global $preferences;
    $tasks['CategoryNumber'] = array();
    foreach ($tasks['Category'] as $category) {
        array_push($tasks['CategoryNumber'], array_search($category, $preferences['categories'], $strict=TRUE));
    }    
}

global $dbm;

$task_numbers = array();

// get completed tasks
$completed = $dbm->tasks_completed();
usort($completed, 'compare_tasks');
$task_numbers['completed_tasks'] = count($completed);
$completed = resort_tasks_from_query($completed);
add_category_numbers($completed);

// get inprogress tasks
$inprogress = $dbm->tasks_assigned();
usort($inprogress, 'compare_tasks');
$task_numbers['inprogress_tasks'] = count($inprogress);
$inprogress = resort_tasks_from_query($inprogress);
add_category_numbers($inprogress);

// get blocked tasks
$blocked = $dbm->tasks_blocked();
usort($blocked, 'compare_tasks');
$task_numbers['blocked_tasks'] = count($blocked);
$blocked = resort_tasks_from_query($blocked);
add_category_numbers($blocked);

// get available tasks
$available = $dbm->tasks_available();
usort($available, 'compare_tasks');
$task_numbers['available_tasks'] = count($available);
$available = resort_tasks_from_query($available);
add_category_numbers($available);

// update info text
$info_text = $task_numbers['available_tasks'].' available, '.$task_numbers['blocked_tasks'].' blocked, '.$task_numbers['inprogress_tasks'].' in progress, '.$task_numbers['completed_tasks'].' completed';
file_put_contents(T_OUT_INFO, $info_text);

// make completed
$output = file_get_contents(T_CACHE_HEADER);
$data = array_merge($preferences, $task_numbers, $completed);
$output .= parse(T_CACHE_COMPLETED, $data);
$output .= file_get_contents(T_CACHE_FOOTER);
file_put_contents(T_OUT_COMPLETED, $output);

// make inprogress
$output = file_get_contents(T_CACHE_HEADER);
$data = array_merge($preferences, $task_numbers, $inprogress);
$output .= parse(T_CACHE_INPROGRESS, $data);
$output .= file_get_contents(T_CACHE_FOOTER);
file_put_contents(T_OUT_INPROGRESS, $output);

// make blocked
$output = file_get_contents(T_CACHE_HEADER);
$data = array_merge($preferences, $task_numbers, $blocked);
$output .= parse(T_CACHE_BLOCKED, $data);
$output .= file_get_contents(T_CACHE_FOOTER);
file_put_contents(T_OUT_BLOCKED, $output);

// make available
$output = file_get_contents(T_CACHE_HEADER);
$data = array_merge($preferences, $task_numbers, $available);
$output .= parse(T_CACHE_AVAILABLE, $data);
$output .= file_get_contents(T_CACHE_FOOTER);
file_put_contents(T_OUT_AVAILABLE, $output);

// parse and adapt css
$data = array('CategoryNumber' => array(), 'CategoryColor' => array());
foreach ($preferences['categories.colors'] as $number => $color) {
    array_push($data['CategoryNumber'], $number);
    array_push($data['CategoryColor'], $color);
}
$output = parse(T_CACHE_CSS, $data);
file_put_contents(T_OUT_CSS, $output);

back_to_overview('Frontend updated.');
