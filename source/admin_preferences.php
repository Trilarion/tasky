<?php

require_once 'internal/base.php';

session_check();

function render_preferences() {
    global $preferences;    

    // header
    $data = array('title' => 'Tasky Manage Update Preferences');
    $output = parse(T_HEADER, $data);

    // preferences
    $data = array('categories' => implode(',', $preferences['categories']),
        'categories.colors' => implode(',', $preferences['categories.colors']));    
    $output .= parse(T_MANAGE_PREFS, $data);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
if (!empty($action)) {
    
    if (isset($preferences['password.hash'])) {
        // get old password
        $password_old = filter_input(INPUT_POST, 'password.old', FILTER_SANITIZE_STRING);    
        $hash_old = password_hash($password_old, PASSWORD_DEFAULT);        
        
        if (!password_verify($password_old, $preferences['password.hash'])) {
            
        }
    }
    
    // if new password is given change it
    $password_new = filter_input(INPUT_POST, 'password.new', FILTER_SANITIZE_STRING);
    $hash_new = password_hash($password_new, PASSWORD_DEFAULT);
    $preferences['password.hash'] = $hash_new;
    
    // update categories
    $categories = filter_input(INPUT_POST, 'categories', FILTER_SANITIZE_STRING);
    $preferences['categories'] = explode(',', $categories);
    
    // update categories.colors
    $categories_colors = filter_input(INPUT_POST, 'categories_colors', FILTER_SANITIZE_STRING);
    $preferences['categories.colors'] = explode(',', $categories_colors);    

    // update preferences
    json_write(F_PREFERENCES, $preferences);    
    
    // relocate to header
    back_to_overview('Preferences updated.');
} else {
    // show preferences form
    render_preferences();    
}


