<?php

require_once 'internal/base.php';

session_check();

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
if (!empty($action)) {
    
    // get form values
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $category = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_STRING);
    if (!in_array($category, $preferences['categories'], $strict=True)) {
        throw new Exception('Category not known.');
    }
    $creation_date = filter_input(INPUT_POST, 'date_creation', FILTER_SANITIZE_STRING);
    $CreationDate = DateTime::createFromFormat('Y-m-d', $creation_date);
    if ($CreationDate === FALSE or $CreationDate === '') {
        throw new Exception('Creation date not valid (YYYY-MM-DD).');
    }
    $Now = new DateTime();
    if ($CreationDate > $Now) {
        throw new Exception('Creation date cannot be in the future!');
    }
    
    // create record in data base
    global $dbm;
    $id = $dbm->create_task($category, $title, $description, $creation_date);
    
    // relocate to header
    back_to_overview("Task $id created.");
} else {
    // show create form
    global $preferences;    
    
    // header
    $data = array('title' => 'Tasky Manage Create');
    $output = parse(T_HEADER, $data);

    // overview
    $data = array('categories' => $preferences['categories'],
        'today' => date('Y-m-d'));
    $output .= parse(T_MANAGE_CREATE, $data);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}
