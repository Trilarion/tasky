<?php

require_once 'internal/base.php';

function render_login_form($error_message = '') {
    
    // header
    $data = array('title' => 'Tasky Log In');
    $output = parse(T_HEADER, $data);

    // login form
    $data = array('error_message' => $error_message);
    $output .= parse(T_LOGIN_FORM, $data);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}

// if there is a correct session already running we don't need to login again
session_start();
$valid_session = isset($_SESSION['id']) ? $_SESSION['id'] === session_id() : FALSE;
if ($valid_session) {
    // forward to overview
    header('Location: admin_overview.php');
    exit();
}

// this only happens before a password was set in the preferences
if (!isset($preferences['password_hash'])) {
    // validate session and forward to update preferences
    $_SESSION['id'] = session_id();
    header('Location: admin_preferences.php');
    exit();
}

// not a valid session 
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
if (!empty($password)) {
    // form has been submitted and we have a password

    // verify password
    if (!password_verify($password, $preferences['password_hash'])) {
        // password wrong, display login form with error message

        render_login_form('Error: Password is incorrect.');
    } else {
        // password right, validate session and relocate
        $_SESSION['id'] = session_id();
        $_SESSION['info'] = 'Log in successful.';
        header('Location: admin_overview.php');
        exit();
    }
} else {
    // form has not been submitted, display login form
    render_login_form();
}