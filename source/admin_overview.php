<?php

require_once 'internal/base.php';

session_check();

function render_overview($info_message) {
    global $dbm; // database manager

    // header
    $data = array('title' => 'Tasky Manage Overview');
    $output = parse(T_HEADER, $data);

    // login form
    $data = array(
        'number_records' => $dbm->number_tasks(),
        'info_message' => $info_message);
    $output .= parse(T_MANAGE_OVERVIEW, $data);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}

if (isset($_SESSION['info'])) {
    $info_message = $_SESSION['info'];
    unset($_SESSION['info']);
} else {
    $info_message = '';
}

render_overview($info_message);

