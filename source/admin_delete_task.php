<?php

require_once 'internal/base.php';

session_check();

$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

if (!empty($id)) {
    
    global $dbm;
    
    if ($dbm->contains_id($id)) {
        $dbm->delete_task($id);
        
        // relocate to overview
        back_to_overview("Task $id deleted.");
    } else {
        
        // relocate to overview        
        back_to_overview("Task $id does not exist, cannot be deleted.");
    }
    
} else {
    back_to_overview('No task ID specified.');
}