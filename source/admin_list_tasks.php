<?php

require_once 'internal/base.php';

session_check();

global $preferences;    
// header
$output = file_get_contents(T_HEADER);

global $dbm;
// overview
$tasks = $dbm->tasks();
$tasks['number_tasks'] = count($tasks['Id']);
$output .= parse(T_MANAGE_LIST, $tasks);

// footer
$output .= file_get_contents(T_FOOTER);

// display
echo $output;