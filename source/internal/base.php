<?php

require_once 'tools.php';

// some constants
define('T', 'internal/template/');
define('T_HEADER', T.'admin_header.html');
define('T_FOOTER', T.'admin_footer.html');
define('T_LOGIN_FORM', T.'login.html');
define('T_MANAGE_OVERVIEW', T.'admin_overview.html');
define('T_MANAGE_EDIT', T.'admin_edit.html');
define('T_MANAGE_CREATE', T.'admin_create.html');
define('T_MANAGE_PREFS', T.'admin_preferences.html');
define('T_MANAGE_LIST', T.'admin_list.html');
define('T_CACHE_HEADER', T.'cache_header.html');
define('T_CACHE_FOOTER', T.'cache_footer.html');
define('T_CACHE_COMPLETED', T.'cache_completed.html');
define('T_CACHE_INPROGRESS', T.'cache_inprogress.html');
define('T_CACHE_BLOCKED', T.'cache_blocked.html');
define('T_CACHE_AVAILABLE', T.'cache_available.html');
define('T_CACHE_CSS', T.'cache.css');
define('T_OUT_COMPLETED', 'tasks_completed.html');
define('T_OUT_INPROGRESS', 'tasks_inprogress.html');
define('T_OUT_BLOCKED', 'tasks_blocked.html');
define('T_OUT_AVAILABLE', 'tasks_available.html');
define('T_OUT_INFO', 'tasks_info.html');
define('T_OUT_CSS', 'css/tasky.css');

define('F_PREFERENCES', 'internal/preferences.json');
define('F_DATABASE', 'internal/tasks.db');

function session_check() {
    // check session id and relocate to login.php if invalid
    session_start();
    $valid_session = isset($_SESSION['id']) ? $_SESSION['id'] === session_id() : FALSE;
    if (!$valid_session) {
        // go back to login
        header('Location: login.php');
        exit();
    }
}

function back_to_overview($info_message=NULL) {
    if ($info_message !== NULL) {
        $_SESSION['info'] = $info_message;
    } else {
        // just in case it is still set, unset it
        unset($_SESSION['info']);
    }
    header('Location: admin_overview.php');
    exit();
}

class Templater
{
    function block_key_replace_callback($matches)
    {
        $id = $matches[1];
        if (!array_key_exists($id, $this->replacements)) {
            throw new Exception('Missing replacement for id {$id}.');
        }
        $replacement = $this->replacements[$id];
        if (is_array($replacement)) {
            return htmlspecialchars($replacement[$this->block_counter]);
        } else {
           return htmlspecialchars($replacement);
        }
    }

    function block_replace_callback($matches)
    {
        $block = $matches[1];
        // find first key
        preg_match('/\{{2} *(.*?) *\}{2}/s', $block, $matches);

        $output = '';
        if ($matches == FALSE) {
            // should contain keys
            throw new Exception('No keys in block.');
        } else {
            // find multiplicity
            $repetitions = count($this->replacements[$matches[1]]);
            for ($this->block_counter = 0; $this->block_counter < $repetitions; $this->block_counter++) {
                $output .= preg_replace_callback('/\{{2} *(.*?) *\}{2}/', array($this, 'block_key_replace_callback'), $block);
            }
        }

        return $output;
    }

    function key_replace_callback($matches)
    {
        $id = $matches[1];
        if (!array_key_exists($id, $this->replacements)) {
            throw new Exception("Missing replacement for id {$id}.");
        }
        return htmlspecialchars($this->replacements[$id]);
    }

    function parse($input, $replacements)
    {
        $this->replacements = $replacements;

        // first parse blocks
        $output = preg_replace_callback('/\{{2} *block:(.*?):endblock *\}{2}/s', array($this, 'block_replace_callback'), $input);

        // second parse non-block keys
        $output = preg_replace_callback('/\{{2} *(.*?) *\}{2}/s', array($this, 'key_replace_callback'), $output);

        // return output
        return $output;
    }
}

// create instance of templater
$tpl = new Templater();

function parse($file, $replacements)
{
    global $tpl;

    $input = file_get_contents($file);
    return $tpl->parse($input, $replacements);
}

function resort_tasks_from_query($tasks) {
    $data = array('Id' => array(), 'Title' => array(), 'Description' => array(), 'Category' => array(), 'CreationDate' => array(), 'Dependencies' => array(), 'AssignedTo' => array(), 'AssignedDate' => array(), 'Completion' => array(), 'CompletionDate' => array());
    foreach ($tasks as $row) {
        foreach ($row as $key => $value) {
            array_push($data[$key], $value);
        }
    }
    return $data;
}

class DatabaseManager
{

    function __construct() {
        $this->db = NULL;
    }

    function exists() {
        return file_exists(F_DATABASE);
    }

    function delete() {
        unlink(F_DATABASE);
    }

    function delete_task($id) {
        $stmt = $this->db->prepare("DELETE FROM 'tasks' WHERE Id=?;");
        $stmt->bindValue(1, $id);
        $stmt->execute();
    }

    function create_task($category, $title, $description, $creation_date) {
        $stmt = $this->db->prepare("INSERT INTO 'tasks' (Category, Title, Description, CreationDate) VALUES (?, ?, ?, ?);");
        $stmt->bindValue(1, $category);
        $stmt->bindValue(2, $title);
        $stmt->bindValue(3, $description);
        $stmt->bindValue(4, $creation_date);
        $stmt->execute();
        return $this->db->lastInsertRowID();
    }

    function update_task($id, $title, $description, $category, $creation_date, $dependencies, $assigned_to, $assigned_date, $completion, $completion_date) {
        $stmt = $this->db->prepare("UPDATE 'tasks' SET Title=?, Description=?, Category=?, CreationDate=?, Dependencies=?, AssignedTo=?, AssignedDate=?, Completion=?, CompletionDate=? WHERE Id=?;");
        $stmt->bindValue(1, $title);
        $stmt->bindValue(2, $description);
        $stmt->bindValue(3, $category);
        $stmt->bindValue(4, $creation_date);
        $stmt->bindValue(5, $dependencies);
        $stmt->bindValue(6, $assigned_to);
        $stmt->bindValue(7, $assigned_date);
        $stmt->bindValue(8, $completion);
        $stmt->bindValue(9, $completion_date);
        $stmt->bindValue(10, $id);
        $stmt->execute();
    }


    function contains_id($id) {
        $stmt = $this->db->prepare("SELECT EXISTS (SELECT 1 FROM 'tasks' WHERE Id=?);");
        $stmt->bindValue(1, $id);
        $return = $stmt->execute();
        $return = $return->fetchArray(SQLITE3_NUM);
        return $return[0] == 1;
    }
    
    function store_tasks_from_query($result) {
        $data = array();
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {        
            array_push($data, $row);
        }
        return $data;                
    }
    
    function tasks() {
        $result = $this->db->query("SELECT * FROM 'tasks';");
        $tasks = $this->store_tasks_from_query($result);
        return resort_tasks_from_query($tasks);
    }

    
    function tasks_completed() {
        $result = $this->db->query("Select * FROM 'tasks' WHERE CompletionDate<>'';");
        return $this->store_tasks_from_query($result);
    }
    
    function tasks_assigned() {
        $result = $this->db->query("Select * FROM 'tasks' WHERE CompletionDate='' AND AssignedDate<>'';");
        return $this->store_tasks_from_query($result);
    }
    
    function tasks_blocked() {
        $result = $this->db->query("Select * FROM 'tasks' WHERE AssignedDate='' AND Dependencies<>'';");
        return $this->store_tasks_from_query($result);
    }
    
    function tasks_available() {
        $result = $this->db->query("Select * FROM 'tasks' WHERE AssignedDate='' AND Dependencies='';");
        return $this->store_tasks_from_query($result);
    }

    function task($id) {
        $stmt = $this->db->prepare("SELECT * FROM 'tasks' WHERE Id=?;");
        $stmt->bindValue(1, $id);
        $return = $stmt->execute();
        return $return->fetchArray(SQLITE3_ASSOC);
    }


    function update_frontend() {

    }
    
    function clear_tasks() {
        $this->db->query("DROP TABLE `tasks`;");
        $this->create_tasks_table();
    }

    function create() {
        if ($this->exists()) {
            throw new Exception('Database file already exists.');
        }
        // create SQLite3 database
        $this->db = new SQLite3(F_DATABASE);
        $this->create_tasks_table();        
    }
    
    function create_tasks_table() {
        $this->db->query("CREATE TABLE `tasks` (
	Id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	Category        TEXT NOT NULL,
	Title           TEXT NOT NULL UNIQUE,
	Description	TEXT NOT NULL,
	CreationDate	TEXT NOT NULL,
	Dependencies	TEXT default '',
	AssignedTo	TEXT default '',
	AssignedDate	TEXT default '',
	Completion	TEXT default '',
	CompletionDate	TEXT default '');");        
    }

    function open() {
        if (!$this->exists()) {
            throw new Exception('Database file does not exist.');
        }
        $this->db = new SQLite3(F_DATABASE);
    }

    function number_tasks() {
        $result = $this->db->query("SELECT COUNT(*) AS 'number_tasks' FROM tasks;");
        $data = $result->fetchArray(SQLITE3_ASSOC);
        return $data['number_tasks'];
    }
}

// create instance of database manager and create database if not yet existing, otherwise open
$dbm = new DatabaseManager();
if (!$dbm->exists()) {
    $dbm->create();
} else {
    $dbm->open();
}

function json_write($file, $data) {
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    file_put_contents($file, $json);
}

function json_read($file) {
    $json = file_get_contents($file);
    $data = json_decode($json, TRUE);
    return $data;
}

// if preferences do not exist, create them
if (!file_exists(F_PREFERENCES)) {
    $preferences = array();
    $preferences['categories'] = array();
    $preferences['categories.colors'] = array();
    json_write(F_PREFERENCES, $preferences);
}

// load preferences
$preferences = json_read(F_PREFERENCES);