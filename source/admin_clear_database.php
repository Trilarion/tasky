<?php

require_once 'internal/base.php';

session_check();

global $dbm;
$dbm->clear_tasks();

back_to_overview('Database cleared..');