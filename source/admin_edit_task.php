<?php

require_once 'internal/base.php';

session_check();

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
if ($action === 'edit') {
    
    // get form values
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);    
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $category = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_STRING);
    if (!in_array($category, $preferences['categories'], $strict=True)) {
        throw new Exception('Category not known.');
    }    
    $creation_date = filter_input(INPUT_POST, 'date_creation', FILTER_SANITIZE_STRING);
    $CreationDate = DateTime::createFromFormat('Y-m-d', $creation_date);
    if ($CreationDate === FALSE) {
        throw new Exception('Date not valid (YYYY-MM-DD).');
    }    
    $dependencies = filter_input(INPUT_POST, 'dependencies', FILTER_SANITIZE_STRING);
    $assigned_to = filter_input(INPUT_POST, 'assigned_to', FILTER_SANITIZE_STRING);
    $assigned_date = filter_input(INPUT_POST, 'date_assigned', FILTER_SANITIZE_STRING);
    $AssignedDate = DateTime::createFromFormat('Y-m-d', $creation_date);
    if ($AssignedDate === FALSE) {
        throw new Exception('Date not valid (YYYY-MM-DD).');
    }  
    $completion = filter_input(INPUT_POST, 'completion', FILTER_SANITIZE_STRING);
    $completion_date = filter_input(INPUT_POST, 'date_completion', FILTER_SANITIZE_STRING);
    $CompletionDate = DateTime::createFromFormat('Y-m-d', $creation_date);
    if ($CompletionDate === FALSE) {
        throw new Exception('Date not valid (YYYY-MM-DD).');
    }    
    
    // more sanity checks
    if ($completion !== '' and $completion_date === '' or $completion === '' and $completion_date !== '') {
        throw new Exception('Completion message and completion date must both be given!');
    }
    if ($assigned_to !== '' and $assigned_date === '' or $assigned_to === '' and $assigned_date !== '') {
        throw new Exception('Assigned to and assigned date must both be given!');
    }
    if ($completion !== '' and $assigned_to === '') {
        throw new Exception('Unassigned task cannot be set completed.');
    }
    if ($assigned_to !== '') {
        $dependencies = '';
        // TODO delete id from all other dependencies
    }
    if ($assigned_date !== '' and $AssignedDate < $CreationDate) {
        throw new Exception('Assigned date cannot pre-date creation date!');
    }
    $Now = new DateTime();
    if ($assigned_date !== '' and $AssignedDate > $Now) {    
        throw new Exception('Assigned date cannot be in the future!');
    }
    if ($completion_date !== '' and $CompletionDate < $AssignedDate) {
        throw new Exception('Completion date cannot pre-date assigned date!');
    }
    if ($completion_date !== '' and $CompletionDate > $Now) {
        throw new Exception('Completion date cannot be in the future!');
    }
    
    // update record in data base
    global $dbm;
    $dbm->update_task($id, $title, $description, $category, $creation_date, $dependencies, $assigned_to, $assigned_date, $completion, $completion_date);
    
    // relocate to header
    back_to_overview("Task $id edited.");
} elseif ($action === 'show') {
    // show edit form
    global $preferences, $dbm;    
    
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
    
    if (!$dbm->contains_id($id)) {
        back_to_overview("Task $id does not exist, cannot be edited.");
    }
    
    // get data
    $task = $dbm->task($id);
    $task['categories'] = $preferences['categories'];
    $task['categories_selected'] = array_fill(0, count($task['categories']), '');
    $task['categories_selected'][array_search($task['Category'], $task['categories'], $strict=TRUE)] = ' selected';

    // header
    $data = array('title' => 'Tasky - Admin - Edit Task');
    $output = parse(T_HEADER, $data);

    // overview
    $output .= parse(T_MANAGE_EDIT, $task);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;   
} else {
    back_to_overview();
}


