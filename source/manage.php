<?php

require_once 'internal/base.php';

function render_overview($info_message) {
    global $dbm; // database manager
    $t = new Templater();

    // header
    $data = array('title' => 'Tasky Manage Overview');
    $output = $t->parse(T_HEADER, $data);

    // login form
    $data = array(
        'number_records' => $dbm->number_tasks(),
        'info_message' => $info_message);
    $output .= $t->parse(T_MANAGE_OVERVIEW, $data);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}

function render_create() {
    $t = new Templater();

    // header
    $data = array('title' => 'Tasky Manage Create');
    $output = $t->parse(T_HEADER, $data);

    // overview
    $output .= file_get_contents(T_MANAGE_CREATE);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}

function render_edit() {
    $t = new Templater();

    // header
    $data = array('title' => 'Tasky Manage Edit');
    $output = $t->parse(T_HEADER, $data);

    // overview
    $output .= file_get_contents(T_MANAGE_EDIT);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}

function render_preferences() {
    global $tpl;    

    // header
    $data = array('title' => 'Tasky Manage Update Preferences');
    $output = $tpl->parse(T_HEADER, $data);

    // preferences
    $output .= file_get_contents(T_MANAGE_PREFS);

    // footer
    $output .= file_get_contents(T_FOOTER);

    // display
    echo $output;
}

function back_to_overview($info_message=NULL) {
    if ($info_message !== NULL) {
        $_SESSION['info'] = $info_message;
    } else {
        // just in case it is still set, unset it
        unset($_SESSION['info']);
    }
    header('Location: manage.php?action=overview');
    exit();
}

// check session id and relocate to login.php if invalid
session_start();
$valid_session = isset($_SESSION['id']) ? $_SESSION['id'] === session_id() : FALSE;
if (!$valid_session) {
    // go back to login
    header('Location: login.php');
    exit();
}

// show the various screens
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
switch ($action) {
    case 'overview':
        if (isset($_SESSION['info'])) {
            $info_message = $_SESSION['info'];
        } else {
            $info_message = '';
        }
        render_overview($info_message);
        break;

    case 'create':
        render_create();
        break;

    case 'creation':
        // TODO create action
        back_to_overview();

    case 'edit':
        render_edit();
        break;

    case 'edition':
        // TODO edit action
        back_to_overview();

    case 'delete':
        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT); // if it pases through this it's an int
        $dbm->delete($id);
        back_to_overview();

    case 'update_cache':
        $dbm->update_frontend();
        back_to_overview('Frontend updated.');

    case 'update_preferences':
        render_preferences();
        break;

    case 'update_preferences_action':

        // read parameters
        $password = $_SESSION['password'];

        $hash = password_hash($password, PASSWORD_DEFAULT);
        $preferences['password_hash'] = $hash;

        // update preferences
        json_write(F_PREFERENCES, $preferences);

        back_to_overview('Preferences updated.');

    case 'logout':
        session_destroy();

        header('Location: login.php');
        exit();

    default:
        throw new Exception("Unknown action: {$action}!");
}
